package com.interview.hao.demo;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import com.interview.hao.demo.model.Asset;
import com.interview.hao.demo.repository.AssetRepository;

@SpringBootApplication
@EnableConfigurationProperties
public class DemoApplication {

	Logger logger = LoggerFactory.getLogger(DemoApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Bean
	public CommandLineRunner seed(AssetRepository repository) {
		return (args) -> {
			logger.info("Start data seeding.");
			repository.save(new Asset(0L, "asset-1", Asset.Type.TRAILER,"Asset A", new Date()));
			repository.save(new Asset(0L, "asset-2", Asset.Type.SEMI, "Asset B", new Date()));
			repository.save(new Asset(0L, "asset-3", Asset.Type.TRUCK, "Asset C", new Date()));
			repository.save(new Asset(0L, "asset-4", Asset.Type.TRAILER, "Asset D", new Date()));
			repository.save(new Asset(0L,"asset-5", Asset.Type.TRUCK, "Asset E", new Date()));
			logger.info("End of data seeding.");
		};
	}
}
