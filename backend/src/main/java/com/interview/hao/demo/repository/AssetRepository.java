package com.interview.hao.demo.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.interview.hao.demo.model.Asset;

@Repository
public interface AssetRepository extends PagingAndSortingRepository<Asset, Long> {
}
