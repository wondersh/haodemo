package com.interview.hao.demo.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.interview.hao.demo.model.Asset;
import com.interview.hao.demo.repository.AssetRepository;

@RestController
@RequestMapping("/assets")
public class AssetController {

	Logger logger = LoggerFactory.getLogger(AssetController.class);

	@Autowired
	private AssetRepository assetRepository;

	@GetMapping
	public ResponseEntity<?> findAll(@RequestParam(value = "sortBy", defaultValue = "id") String sortBy,
			@RequestParam(value = "sortDirection", defaultValue = "ASC") Direction sortDirection) {
		try {
			return ResponseEntity.ok((List<Asset>) assetRepository.findAll(Sort.by(sortDirection, sortBy)));
		} catch (RuntimeException e) {
			logger.error(e.getMessage());
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> getAsset(@PathVariable Long id) {
		try {
			return ResponseEntity.ok(assetRepository.findById(id).orElseThrow(RuntimeException::new));
		} catch (RuntimeException e) {
			logger.error(e.getMessage());
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}

	@PostMapping
	public ResponseEntity<Asset> create(@RequestBody Asset asset) throws URISyntaxException {
		Asset savedSAsset = assetRepository.save(asset);
		return ResponseEntity.created(new URI("/assets/" + savedSAsset.getId())).body(savedSAsset);
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> updateAsset(@PathVariable Long id, @RequestBody Asset asset) {
		try {
			Asset currentAsset = assetRepository.findById(id).orElseThrow(RuntimeException::new);
			currentAsset.setName(asset.getName());
			currentAsset.setType(asset.getType());
			currentAsset.setDescription(asset.getDescription());
			currentAsset = assetRepository.save(currentAsset);

			return ResponseEntity.ok(currentAsset);
		} catch (RuntimeException e) {
			logger.error(e.getMessage());
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteAsset(@PathVariable Long id) {
		assetRepository.deleteById(id);
		return ResponseEntity.ok().build();
	}
}
