package com.interview.hao.demo.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.interview.hao.demo.model.Asset;
import com.interview.hao.demo.model.Asset.Type;
import com.interview.hao.demo.repository.AssetRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(AssetController.class)
public class AssetControllerTest {

	@Autowired
	private MockMvc mvc;
	@MockBean
	private AssetRepository assetRepository;
	@Autowired
	private ObjectMapper objectMapper;

	@Test
	public void shouldReturnAssets_whenGetAssets() throws Exception {
		ArrayList<Asset> assetList = new ArrayList<Asset>();
		assetList.add(new Asset(0L, "A1", Type.SEMI, "D1", new Date()));
		assetList.add(new Asset(0L, "A2", Type.TRAILER, "D2", new Date()));
		assetList.add(new Asset(0L, "A3", Type.TRUCK, "D3", new Date()));
		when(assetRepository.findAll(Mockito.any(Sort.class))).thenReturn(assetList);
		mvc.perform(get("/assets").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$", Matchers.hasSize(3)));
	}

	@Test
	public void shouldReturnBadRequest_whenGetAssetsWithWrongSortingInfo() throws Exception {
		when(assetRepository.findAll(Mockito.any(Sort.class))).thenThrow(new RuntimeException());
		mvc.perform(get("/assets").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
	}

	@Test
	public void shouldReturnAsset_whenGetAssetById() throws Exception {
		Asset asset = new Asset(1L, "A1", Type.SEMI, "D1", new Date());
		when(assetRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(asset));
		mvc.perform(get("/assets/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.name", is("A1")));
	}

	@Test
	public void shouldReturnBadRequest_whenGetAssetWithInvalidId() throws Exception {
		when(assetRepository.findById(Mockito.anyLong())).thenThrow(new RuntimeException());
		mvc.perform(get("/assets/invalid").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
	}

	@Test
	public void shouldCreateAsset_whenPassinAsset() throws Exception {
		Asset asset = new Asset(1L, "A1", Type.SEMI, "D1", new Date());
		when(assetRepository.save(Mockito.any(Asset.class))).thenReturn(asset);
		mvc.perform(
				post("/assets").content(objectMapper.writeValueAsString(asset)).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is2xxSuccessful()).andExpect(jsonPath("$.name", is("A1")));
	}

	@Test
	public void shouldUpdateAsset_whenPassinAsset() throws Exception {
		Asset asset = new Asset(1L, "A1", Type.SEMI, "D1", new Date());
		when(assetRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(asset));
		when(assetRepository.save(Mockito.any(Asset.class))).thenReturn(asset);
		mvc.perform(put("/assets/1").content(objectMapper.writeValueAsString(asset))
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
	
	@Test
	public void shouldReturnBadRequest_whenPassinNonexistingAsset() throws Exception {
		Asset asset = new Asset(1L, "A1", Type.SEMI, "D1", new Date());
		when(assetRepository.findById(Mockito.anyLong())).thenThrow(new RuntimeException());
		mvc.perform(put("/assets/1").content(objectMapper.writeValueAsString(asset))
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
	}
	
	@Test
	public void shouldDeleteAsset_whenPassinAssetId() throws Exception {
		doNothing().when(assetRepository).delete(Mockito.any(Asset.class));
		mvc.perform(delete("/assets/1")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
}
