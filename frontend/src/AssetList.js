import React, { Component } from 'react';
import { Button, ButtonGroup, Container, Table } from 'reactstrap';
import { Link } from 'react-router-dom';
import AppNavbar from './AppNavbar';

class AssetList extends Component {

    constructor(props) {
        super(props);
        this.state = { assets: [] };
        this.remove = this.remove.bind(this);
    }

    componentDidMount() {
        fetch('/assets')
            .then(response => response.json())
            .then(data => this.setState({ assets: data }));
    }
    async remove(id) {
        await fetch(`/assets/${id}`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(() => {
            let updatedassets = [...this.state.assets].filter(i => i.id !== id);
            this.setState({ assets: updatedassets });
        });
    }

    render() {
        const { assets, isLoading } = this.state;

        if (isLoading) {
            return <p>Loading...</p>;
        }

        const assetList = assets.map(asset => {
                return <tr key={asset.id}>
                    <td style={{ whiteSpace: 'nowrap' }}>{asset.name}</td>
            <td>{asset.type}</td>
            <td>
            <ButtonGroup>
            <Button size="sm" color="primary" tag={Link} to={"/portal/assets/" + asset.id}>Edit</Button>
            <Button size="sm" color="danger" onClick={() => this.remove(asset.id)}>Delete</Button>
    </ButtonGroup>
    </td>
    </tr>
});

return (
    <div>
    <AppNavbar />
    <Container fluid>
    <div className="float-right">
    <Button color="success" tag={Link} to="/portal/assets/new">Add asset</Button>
    </div>
    <h3>Asset List</h3>
    <Table className="mt-4">
    <thead>
    <tr>
    <th width="30%">Name</th>
    <th width="30%">Type</th>
    <th width="40%">Actions</th>
    </tr>
    </thead>
    <tbody>
    {assetList}
    </tbody>
    </Table>
    </Container>
    </div>
);
}
}
export default AssetList;