import React, { Component } from 'react';
import './App.css';
import Home from './Home';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import AssetList from './AssetList';
import AssetEdit from "./AssetEdit";

class App extends Component {
    render() {
        return (
            <Router>
            <Switch>
            <Route path='/' exact={true} component={Home} />
                    <Route path='/portal/assets' exact={true} component={AssetList} />
                    <Route path='/portal/assets/:id' component={AssetEdit} />
            </Switch>
            </Router>
    )
}
}

export default App;