#How to run this in docker
Once checkout the code, run the command below to create docker image then run it.
In backend folder, run:
docker build -t haodemo/hao-demo-asset-restapi .
In frontend folder, run:
docker build -t haodemo/hao-demo-asset-portal .
Then go to the project root folder, run:
docker-compose up

Once up, please visit link here:
to check asset api swagger page
http://localhost:8081/swagger-ui.html
to the asset portal 
http://localhost:81/portal/assets